import requests
from datetime import datetime
import my_globals
def getMessage(Decryption = None, Key = None):
    m = requests.get('http://' + my_globals.ip_adress + '/message/').json()
    for i in m:
        if  (not int(i['id']) in my_globals.printed_message) and i['target'] == my_globals.User_logIn:
            if Decryption:
                print Decryption(i['message'], Key)
            else:
                print i['message']
            my_globals.printed_message.append(int(i['id']))

def postMessage(Target, message, Encryption = None):
    keys = requests.get('http://' + my_globals.ip_adress + '/pubKey').json()
    key = False
    for i in keys:
        if i['user'] == Target:
            key = i
    if key and Encryption:
        requests.post('http://' + my_globals.ip_adress + '/message/', data = {
        'user' : my_globals.User_logIn,
        'target' : Target,
        'message' : Encryption(message, key),
        })
    else:
        requests.post('http://' + my_globals.ip_adress + '/message/', data = {
        'user' : my_globals.User_logIn,
        'target' : Target,
        'message' : message,
        })

def logIn(User, pubKey):
    my_globals.User_logIn = User
    requests.post('http://' + my_globals.ip_adress + '/pubKey/', data = {'user' : User, 'key' : pubKey})

def set_ip(ip):
    my_globals.ip_adress = ip


def enc(message, key):
    cypherText = ''
    for i in message:
        cypherText += chr(ord(i) + 1)
    return cypherText

def dec(message, key):
    clearText = ''
    for i in message:
        clearText += chr(ord(i) - 1)
    return clearText

def pgcd(a,b):
    """pgcd(a,b): calcul du 'Plus Grand Commun Diviseur' entre les 2 nombres entiers a et b"""
    if b==0:
        return a
    r=a%b
    return pgcd(b,r)
