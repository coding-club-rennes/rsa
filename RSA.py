#!/usr/bin/python3
#-*-coding:Utf-8 -*

import sys

def PGCD(a, b):
    if a == 0 or b == 0:
        raise ValueError("a is NULL or b is NULL")
    elif a < b:
        a,b = b,a
    r = a % b
    a = b
    b = r
    while r != 0:
        r = a % b
        a = b
        b = r
    return a

try:
    print(PGCD(2, 4))
except ValueError as e:
    print("Error: {}".format(e))
